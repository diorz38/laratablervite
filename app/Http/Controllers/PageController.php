<?php

namespace App\Http\Controllers;

// use ProtoneMedia\Splade\SpladeTable;
// use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    /**
     * Display a listing of the dipas.
     *
     * @return \Illuminate\View\View
     */
    public function dashboard(Request $request)
    {
        return view('home', []);
    }
    public function empty(Request $request)
    {
        return view('tabler.pages.empty', []);
    }

}

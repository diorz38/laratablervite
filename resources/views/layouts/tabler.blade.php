<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta16
* @link https://tabler.io
* Copyright 2018-2022 The Tabler Authors
* Copyright 2018-2022 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dashboard - Tabler - Premium and Open Source dashboard template with responsive and high quality UI.</title>
    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef"/>
    <link rel="apple-touch-icon" href="{{ asset('logo.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <!-- CSS files -->
    @vite(['resources/js/app.js'])
    <link href="{{ asset('dist/css/tabler.min.css?1668287865') }}" rel="stylesheet" />
    {{-- <link href="{{ asset('dist/css/tabler-flags.min.css?1668287865') }}" rel="stylesheet" />
    <link href="{{ asset('dist/css/tabler-payments.min.css?1668287865') }}" rel="stylesheet" />
    <link href="{{ asset('dist/css/tabler-vendors.min.css?1668287865') }}" rel="stylesheet" /> --}}
    <link href="{{ asset('dist/css/demo.min.css?1668287865') }}" rel="stylesheet" />
    <style>
        @import url('https://rsms.me/inter/inter.css');

        :root {
            --tblr-font-sans-serif: Inter, -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
        }
    </style>
</head>

<body>
    @splade

    @include('tabler.modal-example')
    <!-- Libs JS -->
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function (reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>

    <script src="{{ asset('dist/js/demo-theme.min.js?1668287865')}}"></script>
    {{-- <script src="{{ asset('dist/libs/apexcharts/dist/apexcharts.min.js?1668287865') }}" defer></script>
    <script src="{{ asset('dist/libs/jsvectormap/dist/js/jsvectormap.min.js?1668287865') }}" defer></script>
    <script src="{{ asset('dist/libs/jsvectormap/dist/maps/world.js?1668287865') }}" defer></script>
    <script src="{{ asset('dist/libs/jsvectormap/dist/maps/world-merc.js?1668287865') }}" defer></script> --}}
    <!-- Tabler Core -->
    <script src="{{ asset('dist/js/tabler.min.js?1668287865')}}" defer></script>
    {{-- @include('tabler.main-charts') --}}

</body>

</html>

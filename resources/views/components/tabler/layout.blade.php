<div class="page">
    <!-- Sidebar -->
    {{--
    <x-tabler.aside /> --}}
    <!-- Navbar -->
    {{--
    <x-tabler.header /> --}}
    {{--
    <x-tabler.header-condensed /> --}}
    <x-tabler.header-sticky />
    <div class="page-wrapper">
        <!-- Page header -->
        @if(isset($header))
        <header class="page-header d-print-none">
            <div class="container-xl">
                {{ $header }}
            </div>
        </header>
        @endif

        <!-- Page body -->
        <div class="page-body">
            {{ $slot }}
        </div>
        <x-tabler.footer />
    </div>
</div>
